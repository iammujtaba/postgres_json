########################## READING DOCS ##################################
'''  Postgres supports native Json data type since version 9.2 
It provides many function and operators for manupulating JSON Data.

Important Note-- We have to take care of single quotes and double quotes
                Json Object key and value should be written in Double quotes
                Start and end of this {} bracket should be done in Single quotes eg. '{ "name":"Mohd"}'
                -> this command is used to get the result as key format
                ->> this command is used to get the result as text

Postgres SQL Commands....
Creating New Table
    1.Create table customer (id serial not null primary key, 
                            form json not null)
Inserting into Table
    1. Insert Into customer(form) values ('{ "name":"Mohd Mujtaba","address":"khalilabad","mobile":9815118123 }')
    2. Insert into customer(form) values ('{ "product_name":"Classmate Notebook","product_price":100,"brand":"classmate" }')

SELECT QUERY...
    1. SELECT form->>'name' from customer;"
    2. SELECT * from customer;

UPDATE QUERY...
    1. Update customer set form = form::jsonb -'a' || '{ "name":"Mohd Aamir"}' where form->>'name'='Mohd Mujtaba'
    Here we are updating the name inside the json objects.

DELET QUERY
    1. DELETE FROM CUSTOMER WHERE form->>'name'='Mohd Aamir'


'''

# USING PYTHON PSYOPG2

import psycopg2

# Creating Connection
conn = psycopg2.connect(database='ngo', user='mohd',
                        password='mohd', host='127.0.0.1', port='5432')
# Creating Cursor
cursor = conn.cursor()

# Creating Table customer with id as auto increment and form as json object.
# cursor.execute(
#     "Create table customer (id serial not null primary key, form json not null)")
# Inserting json form into customer table which contains Name , Address and Mobile Number
cursor.execute(
    '''Insert into customer(form) values ('{ "name":"Mohd Mujtaba","address":"khalilabad","mobile":9815118123 }')''')

# Inserting json form into customer table which contains Product name , Price and brand
# conn.commit()
cursor.execute(
    '''Insert into customer(form) values ('{ "product_name":"Classmate Notebook","product_price":100,"brand":"classmate" }')''')

# It shows that Postgres JSON can accept any type of form without defining any prototypee
# conn.commit()

# Now Displaying all the records from customer
cursor.execute("select * from customer;")
for item in cursor.fetchall():
    print(item)

# Displaying name of all customers
cursor.execute("SELECT form->>'name' from customer;")
print(cursor.fetchall())


# Upading a row change name in Json Object

cursor.execute(
    '''Update customer set form = form::jsonb -'a' || '{ "name":"Mohd Aamir"}' where form->>'name'='Mohd Mujtaba' ''')

# conn.commit()
print("After Updating Name")
cursor.execute("SELECT form->>'name' from customer;")
print(cursor.fetchall())


# DELETE DATA From JSON Object

cursor.execute("DELETE FROM CUSTOMER WHERE form->>'name'='Mohd Aamir' ")
print("After Deleting Mohd Aamir Name")
cursor.execute("SELECT form->>'name' from customer;")
print(cursor.fetchall())
